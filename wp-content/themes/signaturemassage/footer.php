<footer>

  <div class="footer-info clearfix">
    <div class="padding-wrapper flex-wrapper">

      <?php $currentDate = date("Y"); ?>
      
      <?php if ($currentDate == "2018") {
        $date = $currentDate;
      } else {
        $date = "2018-" . $currentDate;
      }; ?>

      <p class="legal">© <?php echo $date; ?> <?php bloginfo( 'name' ); ?></p>
      <p class="credits">Site design by: <a href="http://hedgeknightcreative.com" target="_blank" rel="nofollow noopener">Hedge Knight Creative</a></p>
    </div>
  </div>

  <a href="#page-top" class="scroll-button">
    <img src="<?php bloginfo( 'template_directory' ); ?>/img/arrow-up.svg" />
  </a>

</footer>

<?php wp_footer(); ?>

</body>
</html>
