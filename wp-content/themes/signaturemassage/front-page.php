<?php get_header(); ?>

  <main class="site-main" role="main">

    <section class="introduction text-block">
      <div class="padding-wrapper">
        <div class="medium-wrapper">
          <div class="aligned-block">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <div class="page-content">

                <?php the_content(); ?>

              </div>

            <?php endwhile; endif; ?>

          </div>
        </div>
      </div>
    </section>

    <section class="text-block">
      <div class="padding-wrapper">
        <div class="medium-wrapper">
          <div class="aligned-block aligned-right">

            <?php the_field( 'text_block' ); ?>

          </div>
        </div>
      </div>
    </section>

    <?php $background = wp_get_attachment_image_src( get_field( 'testimonial_background' ), 'banner' ); ?>

    <section class="testimonial-image-block" style="background-image: url(<?php echo $background[0]; ?>);">
      <div class="padding-wrapper">
        <div class="text-wrapper">

          <div class="testimonial">

            <?php the_field( 'testimonial' ); ?>

          </div>

        </div>
      </div>
    </section>

    <section class="services-section">
      <div class="padding-wrapper">
        <div class="medium-wrapper">

          <div class="text-block">
            <div class="aligned-block">

              <?php the_field( 'services_intro' ); ?>

            </div>
          </div>

          <?php if ( have_rows( 'services' ) ): ?>

            <div class="services">

              <?php while( have_rows( 'services' ) ): the_row(); ?>

                <div class="service">

                  <?php

                    $background = wp_get_attachment_image_src( get_sub_field( 'service_image' ), 'medium' );

                  ?>

                  <?php if( $background ): ?>

                    <div class="service-image" style="background-image: url(<?php echo $background[0]; ?>);"></div>

                  <?php endif; ?>

                  <div class="service-content">

                    <h3><?php the_sub_field( 'service_name' ); ?></h3>

                    <?php the_sub_field( 'service_paragraph' ); ?>

                  </div>

                </div>

              <?php endwhile; ?>

            </div>

          <?php endif; ?>

        </div>
      </div>
    </section>

    <section class="full-page-cta">
      <div class="padding-wrapper">
        <div class="text-wrapper">

          <div class="cta-text">

            <?php the_field( 'cta_text' ); ?>

          </div>

          <a href="https://www.facebook.com/SignatureSportsMassage" class="button button-primary" target="_blank" rel="nofollow noopener"><?php the_field( 'cta_button_text' ); ?></a>

        </div>
      </div>
    </section>

     <section class="pricing text-center">
      <div class="padding-wrapper">
        <div class="medium-wrapper">

          <?php if( have_rows( 'price_cards' ) ): ?>

            <div class="text-wrapper">

              <?php the_field( 'pricing_intro' ); ?>

            </div>

            <div class="price-cards">

              <?php while( have_rows( 'price_cards' ) ): the_row(); ?>

                <?php if( get_sub_field( 'set_as_featured') ) {
                  $featuredClass = 'featured';
                } else {
                  $featuredClass = '';
                } ?>

                <div class="price-card <?php echo $featuredClass; ?>">

                  <div class="price-card-header">

                    <span class="price">

                      <?php the_sub_field( 'price' ); ?>

                    </span>

                  </div>

                  <div class="price-details">

                    <div class="time-length">

                      <?php the_sub_field( 'time_length' ); ?>

                    </div>

                    <div class="price-description">

                      <?php the_sub_field( 'price_details' ); ?>

                    </div>

                  </div>

                </div>

              <?php endwhile; ?>

            </div>

            <div class="text-wrapper">

              <?php the_field( 'disclaimer' ); ?>

            </div>

          <?php endif; ?>

        </div>
      </div>
    </section>

  </main>

<?php get_footer(); ?>