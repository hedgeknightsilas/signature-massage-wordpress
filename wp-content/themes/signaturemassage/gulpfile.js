var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var pump = require('pump');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');

var plumberErrorHandler = { errorHandler: notify.onError({
  title: 'Gulp',
  message: 'Error: <%= error.message %>'
  })
}

gulp.task('sass', function() {
  gulp.src('./css/scss/*.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(gulp.dest('.'))
    .pipe(livereload());
})

gulp.task('js', function() {
  gulp.src('js/src/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('fail'))
    .pipe(concat('main-min.js'))
    .pipe(gulp.dest('js'));
})

gulp.task('compress', function(cb) {
  pump([
    gulp.src('lib/*.js'),
    uglify(),
    gulp.dest('dist')
  ],
  cb
  );
})

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('css/scss/*.scss', ['sass']);
  gulp.watch('js/src/*.js', ['js']);
})

gulp.task('default', ['sass', 'js', 'compress', 'watch']);